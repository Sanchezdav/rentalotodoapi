class AdditionalDetailsController < ApplicationController
  def index
    @subcategory = Subcategory.find_by(slug: params[:subcategory])
    @details = @subcategory.additional_details if !@subcategory.nil?
    render json: @details
  end
end