module PublicationValidableConcern extend ActiveSupport::Concern
  def all_validations
    @validations = PublicationValidation.all
    @validation_progresses = ValidationProgress.where(publication_id: current_publication.id)
    render json: [validations: @validations, progresses: @validation_progresses]
  end

  def validates_publication
    validates_profile
    validates_location
    validates_cover
    validates_text_details
    validates_option_details
  end

  def validates_profile
    profile = current_user.profile
    if (!current_user[:avatar].blank? && !profile[:name].blank? && !profile[:middle].blank? && !profile[:bio].blank? && !profile[:phone].blank? && !profile[:cellphone].blank?)
      save_progress('profile')
    end
  end

  def validates_location
    if (!@publication.location.nil? && !@publication.location.blank?)
      save_progress('location')
    end
  end

  def validates_cover
    if (!@publication[:image].nil? && !@publication[:image].blank?)
      save_progress('cover')
    end
  end
  
  def validates_text_details
    if (@publication.text_details.length >= 1)
      save_progress('characteristics')
    end
  end

  def validates_option_details
    if (@publication.option_details.length >= 1)
      save_progress('options')
    end
  end

  def set_publishable
    publication_validations = PublicationValidation.count
    validation_progresses = ValidationProgress.where(publication_id: @publication.id).count
    if validation_progresses >= publication_validations
      @publication.update_attributes(publishable: true)
    else
      @publication.update_attributes(publishable: false) if @publication.publishable
    end
  end

  def save_progress(validation)
    publication_validation = PublicationValidation.find_by(slug: validation)
    validation_progresses = ValidationProgress.create(
      publication_id: @publication.id,
      publication_validation_id: publication_validation.id
    )
  end
end