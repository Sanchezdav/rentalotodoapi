module PublicationableConcern extend ActiveSupport::Concern
  def current_publication
    @current_publication = @current_publication || Publication.find(params[:publication_id])
  end
end