class ContactCategoriesController < ApplicationController
  def index
    @contacts = ContactCategory.all.order(:name)
    render json: @contacts
  end
end