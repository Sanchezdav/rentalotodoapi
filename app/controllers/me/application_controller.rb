class Me::ApplicationController < ApplicationController
  include PublicationableConcern
  include PublicationValidableConcern
  before_filter :authenticate_user!
end
