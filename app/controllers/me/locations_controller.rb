class Me::LocationsController < Me::ApplicationController
  before_action :set_publication, only: [:index, :create]
  before_action :set_location, only: [:show, :update, :destroy]

  def show
    render json: @location
  end

  def create
    @location = Location.create(publication_id: @publication.id, country: "Mexico")
    if @location.save
      @location.update_attributes(location_params)
      render json: @location
    else
      render :status => :unprocessable_entity, :json => @location.errors
    end
  end

  def update
    if @location.update_attributes(location_params)
      render json: @location
    else
      render :status => :unprocessable_entity, :json => @location.errors
    end
  end

  def destroy
    if @location.destroy
      msg = { status: 200, :message => "Eliminada correctamente!" }
      render :status => :ok, :json => msg
    else
      render :status => :unprocessable_entity,:json => @location.errors
    end
  end

  private
  def location_params
    params.require(:location).permit(:country, :state, :address, :address_line_2, :city, :zip_code)
  end

  def set_location
    @location = Location.find(params[:id])
  end

  def set_publication
    @publication = current_user.publications.find(params[:publication_id])
  end
end