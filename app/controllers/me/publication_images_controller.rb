class Me::PublicationImagesController < Me::ApplicationController
  before_action :set_publication, only: [:index, :create]
  before_action :set_image, only: [:destroy]

  def index
    @images = @publication.images.recents
    render json: @images
  end

  def create
    if !params[:file].blank?
      params[:file].each do |img|
        @image = @publication.images.create(:image => params[:file][img])
        if @image.save
          render json: @image
        else
          render :status => :unprocessable_entity,:json => @image.errors
        end
      end
    end

  end
  
  def destroy
    if @image.destroy
      msg = { status: 200, :message => "Eliminada correctamente!" }
      render :status => :ok, :json => msg
    else
      render :status => :unprocessable_entity,:json => @image.errors
    end
  end

  private
  def set_publication
    @publication = current_user.publications.find(params[:publication_id])
  end

  def set_image
    set_publication
    @image = @publication.images.find(params[:id])
  end

  def image_params
    params.require(:publication_image).permit(:image)
  end
end