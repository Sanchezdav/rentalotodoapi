class Me::PublicationOptionDetailsController < Me::ApplicationController
  before_action :set_publication, only: [:index, :create]
  before_action :set_detail, only: [:update, :destroy]
  after_action :validates_publication, only: [:create, :update]
  after_action :set_publishable, only: [:update]

  def index
    @details = @publication.option_details.all
    render json: @details
  end

  def create
    @my_details = []
    details = @publication.option_details.all
    if !details.nil?
      details.each do |item|
        unless params[:list].map{|a| a[:id]}.include?(item.option_detail_id)
          item.destroy
        end
      end
    end
    params[:list].each do |item|
      @detail = @publication.option_details.find_or_create_by(option_detail_id: item[:id])
      if @detail.save
        @my_details.push(@detail)
      end
    end
    
    if @my_details.length > 0
      render json: @my_details
    end
  end

  def update
    if @detail.option_id != params[:option_id]
      if @detail.update_attributes(option_id: params[:option_id])
        render json: @detail
      else
        render :status => :unprocessable_entity, :json => @detail.errors
      end
    end
  end

  def destroy
    if @detail.destroy
      msg = { status: 200, :message => "Eliminada correctamente!" }
      render :status => :ok, :json => msg
    else
      render :status => :unprocessable_entity,:json => @detail.errors
    end
  end

  private
  def set_publication
    @publication = current_user.publications.find(params[:publication_id])
  end

  def set_detail
    set_publication
    @detail = @publication.option_details.find(params[:id])
  end

  def detail_params
    params.require(:publication_option_detail).permit(:option_detail_id, :option_id)
  end
end