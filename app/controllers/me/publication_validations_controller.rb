class Me::PublicationValidationsController < Me::ApplicationController
  def index
    @validations = PublicationValidation.all
    render json: @validations, publication_id: params[:publication_id]
  end
end