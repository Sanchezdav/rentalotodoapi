class Me::PublicationsController < Me::ApplicationController
  before_action :set_publication, only: [:show, :update, :destroy]
  after_action :validates_publication, only: [:create, :update]
  after_action :set_publishable, only: [:update]

  def index
    @publications = current_user.publications.recents
    render json: @publications
  end

  def show
    render json: @publication
  end

  def create
    @publication = current_user.publications.create(publication_params)

    if @publication.save
      render json: @publication
    else
      render :status => :unprocessable_entity,:json => @publication.errors
    end
  end

  def update
    if params[:file].blank?
      update_publication
    else
      if @publication.update_attributes(:image => params[:file])
        render json: @publication
      else
        render :status => :unprocessable_entity, :json => @publication.errors
      end
    end

  end
  
  def update_publication
    if @publication.update_attributes(publication_params)
      render json: @publication
    else
      render :status => :unprocessable_entity,:json => @publication.errors
    end
  end

  def destroy
    if @publication.destroy
      msg = { status: 200, :message => "Eliminada correctamente!" }
      render :status => :ok, :json => msg
    else
      render :status => :unprocessable_entity,:json => @publication.errors
    end
  end

  private
  def set_publication
    @publication = current_user.publications.find(params[:id])
  end

  def publication_params
    params.require(:publication).permit(:title, :description, :subcategory_id, :published, :price, :publication_type_id, :video)
  end
end