class Me::UserContactsController < Me::ApplicationController
  before_action :set_contact, only: [:update, :destroy]

  def index
    @contacts = current_user.contacts.order(created_at: :desc)
    render json: @contacts
  end

  def create
    category = ContactCategory.find_by(slug: params[:contact][:slug])
    @contact = current_user.contacts.create(
      contact_category_id: category.id,
      contact: params[:contact][:contact]
    )

    if @contact.save
      render json: @contact
    else
      render :status => :unprocessable_entity,:json => @contact.errors
    end
  end

  def update
    if @contact.update_attributes(contact_params)
      render json: @contact
    else
      render :status => :unprocessable_entity,:json => @contact.errors
    end
  end

  def destroy
    if @contact.destroy
      msg = { status: 200, :message => "Eliminado correctamente!" }
      render :status => :ok, :json => msg
    else
      render :status => :unprocessable_entity,:json => @contact.errors
    end
  end

  private
  def set_contact
    @contact = current_user.contacts.find(params[:id])
  end

  def contact_params
    params.require(:user_contact).permit(:contact_category_id, :contact)
  end
end