class PublicationTypesController < ApplicationController
  def index
    @types = PublicationType.all.order(name: :asc)
    render json: @types
  end
end