class PublicationsController < ApplicationController
  def index
    @publications = Publication.published.recents
    render json: @publications
  end

  def show
    @publication = Publication.find(params[:id])
  end
end