class Category < ApplicationRecord
  has_many :subcategories, dependent: :destroy
  validates :name, presence: true, uniqueness: true

  acts_as_paranoid

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]

  def slug_candidates
    [
      :name,
      [:name, :id],
    ]
  end

  def should_generate_new_friendly_id?
    name_changed? || super
  end
end
