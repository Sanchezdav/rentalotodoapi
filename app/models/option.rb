class Option < ApplicationRecord
  belongs_to :option_detail
  has_many :publication_option_details, dependent: :destroy
end
