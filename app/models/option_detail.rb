class OptionDetail < ApplicationRecord
  has_many :subcategory_option_details, dependent: :destroy
  has_many :subcategories, through: :subcategory_option_details, dependent: :destroy
  has_many :publication_option_details, dependent: :destroy
  has_many :options, dependent: :destroy
  validates :name, presence: true, uniqueness: true
  default_scope { order(name: :asc) }

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]

  def slug_candidates
    [
      :name,
      [:name, :id],
    ]
  end

  def should_generate_new_friendly_id?
    name_changed? || super
  end
end
