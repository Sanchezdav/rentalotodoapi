class Publication < ApplicationRecord
  belongs_to :authorable, polymorphic: true
  belongs_to :subcategory
  belongs_to :publication_type
  has_one :category, through: :subcategory
  has_one :location, dependent: :destroy
  has_many :images, class_name: "PublicationImage", dependent: :destroy
  has_many :text_details, class_name: "PublicationTextDetail", dependent: :destroy
  has_many :option_details, class_name: "PublicationOptionDetail", dependent: :destroy
  has_many :additional_details, class_name: "PublicationPlusDetail", dependent: :destroy
  has_many :validation_progresses, dependent: :destroy
  before_create :set_defaults
  # after_create :my_location
  validates :title, presence: true, length: {in: 5..50}
  validates :subcategory_id, presence: true
  validates :publication_type_id, presence: true
  validates :alias, uniqueness: { message: "El alias ya ha sido tomado, modifíquelo por favor!" }, allow_blank: true

  mount_uploader :image, PublicationUploader
  validates_integrity_of  :image
  validates_processing_of :image

  acts_as_paranoid

  extend FriendlyId
  friendly_id :alias, use: [:slugged, :finders]

  scope :recents, -> { order(created_at: :desc) }
  scope :published, -> { where(published: true) }

  private
  def set_defaults
    self.alias = SecureRandom.hex(10)
    self.slug = self.alias
  end

  # def my_location
  #   location = Location.create(:publication_id => self.id)
  # end
end
