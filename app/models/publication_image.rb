class PublicationImage < ApplicationRecord
  belongs_to :publication

  mount_uploader :image, ImagesUploader

  scope :recents, -> { order(created_at: :desc) }
end
