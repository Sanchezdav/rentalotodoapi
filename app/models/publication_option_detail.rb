class PublicationOptionDetail < ApplicationRecord
  belongs_to :publication
  belongs_to :option_detail
  belongs_to :option
end
