class PublicationPlusDetail < ApplicationRecord
  belongs_to :publication
  belongs_to :additional_detail
end
