class PublicationTextDetail < ApplicationRecord
  belongs_to :publication
  belongs_to :text_detail
end
