class PublicationValidation < ApplicationRecord
  has_many :validation_progresses, dependent: :destroy
  has_one :progress, class_name: 'ValidationProgress'
  default_scope { order(position: :asc) }
  validates :rule, presence: true, uniqueness: true
end
