class Subcategory < ApplicationRecord
  belongs_to :category
  has_many :publications, dependent: :destroy
  has_many :subcategory_text_details, dependent: :destroy
  has_many :text_details, through: :subcategory_text_details, dependent: :destroy
  has_many :subcategory_option_details, dependent: :destroy
  has_many :option_details, through: :subcategory_option_details, dependent: :destroy
  has_many :subcategory_additional_details, dependent: :destroy
  has_many :additional_details, through: :subcategory_additional_details, dependent: :destroy
  validates :name, presence: true, uniqueness: { scope: :category_id }

  acts_as_paranoid

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]

  def slug_candidates
    [
      :name,
      [:name, :id],
    ]
  end

  def should_generate_new_friendly_id?
    name_changed? || super
  end
end
