class SubcategoryAdditionalDetail < ApplicationRecord
  belongs_to :subcategory
  belongs_to :additional_detail
end
