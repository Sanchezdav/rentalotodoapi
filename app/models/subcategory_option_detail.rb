class SubcategoryOptionDetail < ApplicationRecord
  belongs_to :subcategory
  belongs_to :option_detail
end
