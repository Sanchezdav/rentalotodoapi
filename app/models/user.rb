class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_one :profile, :dependent => :destroy
  has_many :publications, :as => :authorable, dependent: :destroy
  has_many :contacts, class_name: "UserContact", :dependent => :destroy
  has_many :contact_categories, through: :contacts, :dependent => :destroy
  after_create :my_profile
  validates :email, presence: false, uniqueness: true, format: {with:Devise.email_regexp, allow_blank: true}
  mount_uploader :avatar, AvatarUploader
  validates_integrity_of  :avatar
  validates_processing_of :avatar

  private
  def my_profile
    profile = Profile.create(:user_id => self.id)
  end
end
