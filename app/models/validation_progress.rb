class ValidationProgress < ApplicationRecord
  belongs_to :publication
  belongs_to :publication_validation
  validates :publication_id, uniqueness: {scope: :publication_validation_id}
end
