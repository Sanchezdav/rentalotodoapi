class AdditionalDetailSerializer < ActiveModel::Serializer
  attributes :id, :name, :slug, :group, :included

  def included
    object.publication_plus_details.any? {|item| object.id == item.additional_detail_id} if !object.publication_plus_details.nil?
  end
end
