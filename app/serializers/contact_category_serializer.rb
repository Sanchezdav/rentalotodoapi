class ContactCategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :icon, :slug
end
