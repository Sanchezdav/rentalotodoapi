class LocationSerializer < ActiveModel::Serializer
  attributes :id, :country, :state, :address, :address_line_2, :city, :zip_code
end
