class OptionDetailSerializer < ActiveModel::Serializer
  attributes :id, :name, :slug, :included, :options

  def options
    object.options
  end

  def included
    object.publication_option_details.any? {|item| object.id == item.option_detail_id} if !object.publication_option_details.nil?
  end
end
