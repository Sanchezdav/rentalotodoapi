class PublicationImageSerializer < ActiveModel::Serializer
  attributes :id, :image
  belongs_to :publication
end
