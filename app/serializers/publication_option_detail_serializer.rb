class PublicationOptionDetailSerializer < ActiveModel::Serializer
  attributes :id, :publication_id, :option_detail_id, :option_id, :option_detail

  belongs_to :publication
  belongs_to :option

  def option_detail
    OptionDetailSerializer.new(object.option_detail).attributes
  end
end
