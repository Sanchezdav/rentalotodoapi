class PublicationPlusDetailSerializer < ActiveModel::Serializer
  attributes :id, :publication_id, :additional_detail_id, :answer

  belongs_to :publication
  belongs_to :additional_detail
end
