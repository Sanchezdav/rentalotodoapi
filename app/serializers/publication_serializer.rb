class PublicationSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  attributes :id, :title, :description, :subcategory_id, :publishable, :published, :price, :alias, :slug, :image, :created_at, :publication_type_id, :video
  belongs_to :category
  belongs_to :subcategory
  belongs_to :authorable, polymorphic: true
  has_one :location

  # def created_at
  #   object.created_at = time_ago_in_words(object.created_at)
  # end

  # def saved
  #   if !current_user.nil?
  #     current_user.favorites.any? {|favorite| object.id == favorite.favoritable_id and favorite.favoritable_type == "Article"} if !current_user.favorites.nil?
  #   else
  #     nil
  #   end
  # end
end
