class PublicationTextDetailSerializer < ActiveModel::Serializer
  attributes :id, :publication_id, :text_detail_id, :answer

  belongs_to :publication
  belongs_to :text_detail
end
