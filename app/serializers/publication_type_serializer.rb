class PublicationTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :slug
end
