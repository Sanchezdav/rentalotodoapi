class PublicationValidationSerializer < ActiveModel::Serializer
  include PublicationableConcern
  attributes :id, :rule, :slug, :position, :completed

  def completed
    object.validation_progresses.any? {|item| object.id == item.publication_validation_id && @instance_options[:publication_id].to_i == item.publication_id}
  end
end
