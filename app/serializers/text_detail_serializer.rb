class TextDetailSerializer < ActiveModel::Serializer
  attributes :id, :name, :slug, :included

  def included
    object.publication_text_details.any? {|item| object.id == item.text_detail_id} if !object.publication_text_details.nil?
  end
end
