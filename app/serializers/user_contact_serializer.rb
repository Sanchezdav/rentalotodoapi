class UserContactSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :contact_category_id, :contact

  belongs_to :user
  belongs_to :contact_category
end
