class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :avatar, :is_admin, :profile

  def profile
    ProfileSerializer.new(object.profile).attributes
  end
end
