class ValidationProgressSerializer < ActiveModel::Serializer
  attributes :id, :publication_id, :publication_validation_id
  belongs_to :publication
  belongs_to :publication_validation
end
