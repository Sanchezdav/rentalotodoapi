Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth', :controllers => { omniauth_callbacks: 'users/omniauth_callbacks'}

  scope "me", module: "me", defaults:{format: 'json'} do
    get '', to: 'profiles#show'
    put '/:id', to: 'profiles#update'
    resources :publications do
      resources :publication_images, only: [:index, :create, :destroy]
      resources :locations, only: [:show, :create, :update, :destroy]
      resources :publication_text_details, only: [:index, :create, :update, :destroy]
      resources :publication_option_details, only: [:index, :create, :update, :destroy]
      resources :publication_plus_details, only: [:index, :create, :update, :destroy]
      resources :validation_progresses, only: [:index]
      resources :publication_validations, only: [:index]
    end
    resources :user_contacts, only: [:index, :create, :update, :destroy]
  end

  resources :publications, only: [:index, :show]
  resources :categories, only: [:index], defaults:{format: 'json'}
  resources :subcategories, only: [:index], defaults:{format: 'json'}
  resources :publication_types, only: [:index], defaults:{format: 'json'}
  resources :text_details, only: [:index], defaults:{format: 'json'}
  resources :option_details, only: [:index], defaults:{format: 'json'}
  resources :additional_details, only: [:index], defaults:{format: 'json'}
  resources :contact_categories, only: [:index], defaults:{format: 'json'}
end
