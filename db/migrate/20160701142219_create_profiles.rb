class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true, on_delete: :cascade
      t.string :name
      t.string :middle
      t.string :last
      t.text :bio
      t.string :alias
      t.string :phone
      t.string :cellphone

      t.timestamps
    end
  end
end
