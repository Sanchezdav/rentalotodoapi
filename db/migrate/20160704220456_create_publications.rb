class CreatePublications < ActiveRecord::Migration[5.0]
  def change
    create_table :publications do |t|
      t.string :title
      t.text :description
      t.references :authorable, polymorphic: true, index: true
      t.references :subcategory, foreign_key: true, on_delete: :cascade
      t.boolean :publishable, default: false
      t.boolean :published, default: false
      t.string :alias
      t.string :slug, unique: true
      t.string :image
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
