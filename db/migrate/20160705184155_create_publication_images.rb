class CreatePublicationImages < ActiveRecord::Migration[5.0]
  def change
    create_table :publication_images do |t|
      t.references :publication, foreign_key: true, on_delete: :cascade
      t.string :image

      t.timestamps
    end
  end
end
