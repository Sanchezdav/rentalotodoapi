class CreatePublicationTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :publication_types do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end
