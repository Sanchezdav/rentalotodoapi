class AddVideoToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :video, :string
  end
end
