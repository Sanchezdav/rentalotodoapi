class CreateSubcategoryTextDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :subcategory_text_details do |t|
      t.references :subcategory, foreign_key: true, on_delete: :cascade
      t.references :text_detail, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
