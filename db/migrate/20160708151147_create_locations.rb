class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.references :publication, foreign_key: true, on_delete: :cascade
      t.string :country
      t.string :state
      t.string :address
      t.string :address_line_2
      t.string :city
      t.string :zip_code

      t.timestamps
    end
  end
end
