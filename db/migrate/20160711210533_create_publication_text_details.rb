class CreatePublicationTextDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :publication_text_details do |t|
      t.references :publication, foreign_key: true, on_delete: :cascade
      t.references :text_detail, foreign_key: true, on_delete: :cascade
      t.string :answer

      t.timestamps
    end
  end
end
