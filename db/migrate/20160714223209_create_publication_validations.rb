class CreatePublicationValidations < ActiveRecord::Migration[5.0]
  def change
    create_table :publication_validations do |t|
      t.string :rule
      t.string :slug, index: true
      t.integer :position, index: true

      t.timestamps
    end
  end
end
