class CreateValidationProgresses < ActiveRecord::Migration[5.0]
  def change
    create_table :validation_progresses do |t|
      t.references :publication, foreign_key: true, index: true, on_delete: :cascade
      t.references :publication_validation, foreign_key: true, index: true, on_delete: :cascade

      t.timestamps
    end
  end
end
