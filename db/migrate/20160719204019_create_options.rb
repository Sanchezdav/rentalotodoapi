class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
      t.references :option_detail, foreign_key: true, on_delete: :cascade
      t.string :option

      t.timestamps
    end
  end
end
