class CreatePublicationOptionDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :publication_option_details do |t|
      t.references :publication, foreign_key: true, on_delete: :cascade
      t.references :option, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
