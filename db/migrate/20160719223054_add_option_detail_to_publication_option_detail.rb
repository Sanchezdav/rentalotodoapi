class AddOptionDetailToPublicationOptionDetail < ActiveRecord::Migration[5.0]
  def change
    add_reference :publication_option_details, :option_detail, foreign_key: true, on_delete: :cascade
  end
end
