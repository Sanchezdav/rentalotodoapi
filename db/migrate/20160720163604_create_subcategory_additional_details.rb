class CreateSubcategoryAdditionalDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :subcategory_additional_details do |t|
      t.references :subcategory, foreign_key: true, on_delete: :cascade
      t.references :additional_detail, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
