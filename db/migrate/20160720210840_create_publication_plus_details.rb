class CreatePublicationPlusDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :publication_plus_details do |t|
      t.references :publication, foreign_key: true, on_delete: :cascade
      t.references :additional_detail, foreign_key: true, on_delete: :cascade
      t.boolean :answer, default: true

      t.timestamps
    end
  end
end
