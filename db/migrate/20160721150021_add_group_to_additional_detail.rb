class AddGroupToAdditionalDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :additional_details, :group, :string
  end
end
