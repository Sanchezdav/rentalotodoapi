class AddPriceToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :price, :decimal, default: 0, precision: 12, scale: 2
  end
end
