class CreateContactCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_categories do |t|
      t.string :name
      t.string :icon
      t.string :slug

      t.timestamps
    end
  end
end
