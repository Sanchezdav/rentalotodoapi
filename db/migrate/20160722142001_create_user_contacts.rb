class CreateUserContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :user_contacts do |t|
      t.references :user, foreign_key: true, on_delete: :cascade
      t.references :contact_category, foreign_key: true, on_delete: :cascade
      t.string :contact

      t.timestamps
    end
  end
end
