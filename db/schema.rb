# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160722142001) do

  create_table "additional_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "group"
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "icon"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "publication_id"
    t.string   "country"
    t.string   "state"
    t.string   "address"
    t.string   "address_line_2"
    t.string   "city"
    t.string   "zip_code"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["publication_id"], name: "index_locations_on_publication_id", using: :btree
  end

  create_table "option_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "options", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "option_detail_id"
    t.string   "option"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["option_detail_id"], name: "index_options_on_option_detail_id", using: :btree
  end

  create_table "profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "middle"
    t.string   "last"
    t.text     "bio",        limit: 65535
    t.string   "alias"
    t.string   "phone"
    t.string   "cellphone"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "publication_images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "publication_id"
    t.string   "image"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["publication_id"], name: "index_publication_images_on_publication_id", using: :btree
  end

  create_table "publication_option_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "publication_id"
    t.integer  "option_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "option_detail_id"
    t.index ["option_detail_id"], name: "index_publication_option_details_on_option_detail_id", using: :btree
    t.index ["option_id"], name: "index_publication_option_details_on_option_id", using: :btree
    t.index ["publication_id"], name: "index_publication_option_details_on_publication_id", using: :btree
  end

  create_table "publication_plus_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "utf8_general_ci" do |t|
    t.integer  "publication_id"
    t.integer  "additional_detail_id"
    t.boolean  "answer",               default: true
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["additional_detail_id"], name: "index_publication_plus_details_on_additional_detail_id", using: :btree
    t.index ["publication_id"], name: "index_publication_plus_details_on_publication_id", using: :btree
  end

  create_table "publication_text_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "publication_id"
    t.integer  "text_detail_id"
    t.string   "answer"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["publication_id"], name: "index_publication_text_details_on_publication_id", using: :btree
    t.index ["text_detail_id"], name: "index_publication_text_details_on_text_detail_id", using: :btree
  end

  create_table "publication_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "utf8_general_ci" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
  end

  create_table "publication_validations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "rule"
    t.string   "slug"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_publication_validations_on_position", using: :btree
    t.index ["slug"], name: "index_publication_validations_on_slug", using: :btree
  end

  create_table "publications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.text     "description",         limit: 65535
    t.string   "authorable_type"
    t.integer  "authorable_id"
    t.integer  "subcategory_id"
    t.boolean  "publishable",                                                default: false
    t.boolean  "published",                                                  default: false
    t.string   "alias"
    t.string   "slug"
    t.string   "image"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                 null: false
    t.datetime "updated_at",                                                                 null: false
    t.integer  "publication_type_id"
    t.string   "video"
    t.decimal  "price",                             precision: 12, scale: 2, default: "0.0"
    t.index ["authorable_type", "authorable_id"], name: "index_publications_on_authorable_type_and_authorable_id", using: :btree
    t.index ["publication_type_id"], name: "index_publications_on_publication_type_id", using: :btree
    t.index ["subcategory_id"], name: "index_publications_on_subcategory_id", using: :btree
  end

  create_table "subcategories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "category_id"
    t.string   "name"
    t.string   "slug"
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id"], name: "index_subcategories_on_category_id", using: :btree
  end

  create_table "subcategory_additional_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "subcategory_id"
    t.integer  "additional_detail_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["additional_detail_id"], name: "index_subcategory_additional_details_on_additional_detail_id", using: :btree
    t.index ["subcategory_id"], name: "index_subcategory_additional_details_on_subcategory_id", using: :btree
  end

  create_table "subcategory_option_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "subcategory_id"
    t.integer  "option_detail_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["option_detail_id"], name: "index_subcategory_option_details_on_option_detail_id", using: :btree
    t.index ["subcategory_id"], name: "index_subcategory_option_details_on_subcategory_id", using: :btree
  end

  create_table "subcategory_text_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "subcategory_id"
    t.integer  "text_detail_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["subcategory_id"], name: "index_subcategory_text_details_on_subcategory_id", using: :btree
    t.index ["text_detail_id"], name: "index_subcategory_text_details_on_text_detail_id", using: :btree
  end

  create_table "text_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "contact_category_id"
    t.string   "contact"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["contact_category_id"], name: "index_user_contacts_on_contact_category_id", using: :btree
    t.index ["user_id"], name: "index_user_contacts_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email"
    t.string   "provider",                             default: "email", null: false
    t.string   "uid",                                  default: "",      null: false
    t.string   "avatar"
    t.boolean  "is_admin",                             default: false
    t.string   "encrypted_password",                   default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  create_table "validation_progresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "publication_id"
    t.integer  "publication_validation_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["publication_id"], name: "index_validation_progresses_on_publication_id", using: :btree
    t.index ["publication_validation_id"], name: "index_validation_progresses_on_publication_validation_id", using: :btree
  end

  add_foreign_key "locations", "publications"
  add_foreign_key "options", "option_details"
  add_foreign_key "profiles", "users"
  add_foreign_key "publication_images", "publications"
  add_foreign_key "publication_option_details", "option_details"
  add_foreign_key "publication_option_details", "options"
  add_foreign_key "publication_option_details", "publications"
  add_foreign_key "publication_plus_details", "additional_details"
  add_foreign_key "publication_plus_details", "publications"
  add_foreign_key "publication_text_details", "publications"
  add_foreign_key "publication_text_details", "text_details"
  add_foreign_key "publications", "publication_types"
  add_foreign_key "publications", "subcategories"
  add_foreign_key "subcategories", "categories"
  add_foreign_key "subcategory_additional_details", "additional_details"
  add_foreign_key "subcategory_additional_details", "subcategories"
  add_foreign_key "subcategory_option_details", "option_details"
  add_foreign_key "subcategory_option_details", "subcategories"
  add_foreign_key "subcategory_text_details", "subcategories"
  add_foreign_key "subcategory_text_details", "text_details"
  add_foreign_key "user_contacts", "contact_categories"
  add_foreign_key "user_contacts", "users"
  add_foreign_key "validation_progresses", "publication_validations"
  add_foreign_key "validation_progresses", "publications"
end
